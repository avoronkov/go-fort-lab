package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Interpret struct {
	scanner     *bufio.Scanner
	tokenBuffer []string
	stack       *Stack
	funcs       map[string]StackFunc
	out         io.Writer
}

func NewInterpret(r io.Reader, w io.Writer) *Interpret {
	i := &Interpret{}
	i.scanner = bufio.NewScanner(r)
	i.out = w
	i.scanner.Split(bufio.ScanLines)
	i.stack = NewStack()
	i.funcs = map[string]StackFunc{
		"dup":   StackFn(Dup),
		"drop":  StackFn(Drop),
		"swap":  StackFn(Swap),
		"rot":   StackFn(Rot),
		"+":     StackFn(Plus),
		"-":     StackFn(Minus),
		"*":     StackFn(Multiply),
		"<":     StackFn(Less),
		"!":     StackFn(Not),
		"print": StackFnOut(Print),
		"dump":  StackFnOut(Dump),
	}
	return i
}

func (i *Interpret) Run() (err error) {
	for {
		// read token
		stackFn, err := i.readTokenFn()
		if err != nil {
			return err
		}
		if stackFn == nil {
			// EOF
			break
		}
		// execute token
		if err = stackFn.Call(i.stack, i.out); err != nil {
			return err
		}
	}
	return
}

func (i *Interpret) nextToken() (string, error) {
	if len(i.tokenBuffer) > 0 {
		result := i.tokenBuffer[0]
		i.tokenBuffer = i.tokenBuffer[1:]
		return result, nil
	}
	if !i.scanner.Scan() {
		if err := i.scanner.Err(); err != nil {
			return "", err
		}
		// EOF
		return "", nil
	}
	line := strings.TrimSpace(i.scanner.Text())
	if line == "" || line[0] == '#' {
		// Empty line or comment: skip this line
		return i.nextToken()
	}
	fields := strings.Fields(line)
	result := fields[0]
	i.tokenBuffer = fields[1:]
	return result, nil
}

func (i *Interpret) readTokenFn() (StackFunc, error) {
	token, err := i.nextToken()
	if err != nil {
		return nil, err
	} else if token == "" {
		// program is finished
		return nil, nil
	}
	if token == "]" {
		return nil, fmt.Errorf("Unexpected token \"]\"")
	}
	if token == "[" {
		return i.readLoopFn()
	}
	if token == "define" {
		name, sf, err := i.readDefineFunc()
		if err != nil {
			return nil, err
		}
		i.funcs[name] = sf
		return &NoopFunc{}, nil
	}
	if fn, ok := i.funcs[token]; ok {
		return fn, nil
	}
	if n, err := strconv.Atoi(token); err == nil {
		return &NumberFunc{n}, nil
	}
	return nil, fmt.Errorf("Unknown token %q", token)
}

func (i *Interpret) readLoopFn() (StackFunc, error) {
	lp := &LoopFunc{}

	for {
		token, err := i.nextToken()
		if err != nil {
			return nil, err
		}
		if token == "" {
			break
		}
		if token == "]" {
			return lp, nil
		}
		if token == "[" {
			sf, err := i.readLoopFn()
			if err != nil {
				return nil, err
			}
			lp.loop = append(lp.loop, sf)
			continue
		}
		if fn, ok := i.funcs[token]; ok {
			lp.loop = append(lp.loop, fn)
			continue
		}
		if n, err := strconv.Atoi(token); err == nil {
			lp.loop = append(lp.loop, &NumberFunc{n})
			continue
		}
		return nil, fmt.Errorf("Unknown token %q", token)
	}
	return nil, fmt.Errorf("Unexpected EOF while processing loop")
}

func (i *Interpret) readDefineFunc() (string, StackFunc, error) {
	// let's make a define with Loop :)
	def := &SliceFunc{}

	name, err := i.nextToken()
	if err != nil {
		return "", nil, err
	}
	if name == "" {
		return "", nil, fmt.Errorf("Unexpected EOF while processing define (name)")
	}

	for {
		token, err := i.nextToken()
		if err != nil {
			return "", nil, err
		}
		if token == "" {
			break
		}
		if token == ";" {
			return name, def, nil
		}
		if token == "[" {
			sf, err := i.readLoopFn()
			if err != nil {
				return name, nil, err
			}
			def.slice = append(def.slice, sf)
			continue
		}
		if fn, ok := i.funcs[token]; ok {
			def.slice = append(def.slice, fn)
			continue
		}
		if n, err := strconv.Atoi(token); err == nil {
			def.slice = append(def.slice, &NumberFunc{n})
			continue
		}
	}
	return "", nil, fmt.Errorf("Unexpected EOF while processing define")
}

// StackFunc is interface of anything that can operate with stack
type StackFunc interface {
	Call(*Stack, io.Writer) error
}

type StackFn func(*Stack) error

func (f StackFn) Call(s *Stack, w io.Writer) error {
	if dumpStack {
		defer fmt.Fprintf(os.Stderr, "> %v\n", s)
	}
	return f(s)
}

type StackFnOut func(*Stack, io.Writer) error

func (f StackFnOut) Call(s *Stack, w io.Writer) error {
	return f(s, w)
}

// LoopFunc is loop processor over the stack
type LoopFunc struct {
	loop []StackFunc
}

func (l *LoopFunc) Call(stack *Stack, w io.Writer) error {
	for {
		value, err := stack.Pop()
		if err != nil {
			return err
		}
		if value == 0 {
			return nil
		}
		for _, f := range l.loop {
			if err = f.Call(stack, w); err != nil {
				return err
			}
		}
	}
	return nil
}

// NumberFunc is function that places number on a stack
type NumberFunc struct {
	n int
}

func (n *NumberFunc) Call(s *Stack, w io.Writer) error {
	s.Push(n.n)
	return nil
}

// Define
type SliceFunc struct {
	slice []StackFunc
}

func (s *SliceFunc) Call(stack *Stack, w io.Writer) error {
	for _, fn := range s.slice {
		if err := fn.Call(stack, w); err != nil {
			return err
		}
	}
	return nil
}

// Noop
type NoopFunc struct{}

func (n *NoopFunc) Call(s *Stack, w io.Writer) error {
	return nil
}
