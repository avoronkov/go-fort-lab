package main

import (
	"io"
	"os"
)

func MultiFileReader(files ...string) (io.ReadCloser, error) {
	r := &multiFileReader{
		files:   files,
		current: 0,
	}
	var err error
	r.reader, err = r.open(files[r.current])
	if err != nil {
		return nil, err
	}
	return r, nil
}

type multiFileReader struct {
	files   []string
	current int
	reader  io.Reader
}

func (r *multiFileReader) open(file string) (io.Reader, error) {
	if file == "-" {
		return os.Stdin, nil
	}
	f, err := os.Open(file)
	return f, err
}

func (r *multiFileReader) Read(p []byte) (int, error) {
	n, err := r.reader.Read(p)
	if err == io.EOF {
		if rc, ok := r.reader.(io.ReadCloser); ok {
			if e := rc.Close(); e != nil {
				return 0, e
			}
		}
		r.current++
		if r.current >= len(r.files) {
			return 0, io.EOF
		}
		r.reader, err = r.open(r.files[r.current])
		if err != nil {
			return 0, err
		}
		return r.Read(p)
	}
	return n, err
}

func (r *multiFileReader) Close() error {
	if rc, ok := r.reader.(io.ReadCloser); ok {
		return rc.Close()
	}
	return nil
}
