package main

import (
	"bytes"
	"reflect"
	"strconv"
	"strings"
	"testing"
)

var examples = []struct {
	input  string
	output string
}{
	{
		"2 3 + print",
		"5\n",
	},
	{
		"5 1 swap dup [ dup rot * swap 1 - dup ] drop print",
		"120\n",
	},
	{
		"0 1 < [ 1 print 0 ]",
		"1\n",
	},
	{
		`5 7
		dup rot dup rot 1 rot rot  <
		[ drop swap drop 0 0 ]
		[ drop 0 ]
		print`,
		"5\n",
	},
	{
		`8 6
		dup rot dup rot 1 rot rot  <
		[ drop swap drop 0 0 ]
		[ drop 0 ]
		print`,
		"6\n",
	},
	{
		`5
		dup [ print 1 - dup ]`,
		"5\n4\n3\n2\n1\n",
	},
	{
		`5
		dup [ dup dup [ print 1 - dup ] drop 1 - dup ]`,
		"5\n4\n3\n2\n1\n4\n3\n2\n1\n3\n2\n1\n2\n1\n1\n",
	},
	{
		`define min
		  dup rot dup rot 1 rot rot  <
		  [ drop swap drop 0 0 ]
		  [ drop 0 ] ;

		7 5 min print`,
		"5\n",
	},
}

func TestExamples(t *testing.T) {
	for _, example := range examples {
		input := strings.NewReader(example.input)
		var output bytes.Buffer
		i := NewInterpret(input, &output)
		if err := i.Run(); err != nil {
			t.Errorf("Error while executing `%v`: %v", example.input, err)
			continue
		}
		if act := output.String(); act != example.output {
			t.Errorf("Incorrect output from `%v`: expected `%v`, actual `%v`", example.input, example.output, act)
			continue
		}
		t.Logf("[OK] Example `%v` (= `%v`)", example.input, strings.Trim(example.output, " \n"))
	}
}

func TestNextToken(t *testing.T) {
	testdata := []struct {
		in  string
		out []string
	}{
		{
			in: `one two   
# comment
three

four five six
seven

`,
			out: []string{"one", "two", "three", "four", "five", "six", "seven"},
		},
		{
			in:  "0 1 < [ 1 print 0 ]",
			out: []string{"0", "1", "<", "[", "1", "print", "0", "]"},
		},
	}

	for i, test := range testdata {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			i := NewInterpret(strings.NewReader(test.in), nil)

			act := []string{}

			for {
				token, err := i.nextToken()
				if err != nil {
					t.Fatalf("nextToken failed: %v", err)
				}
				if token == "" {
					break
				}
				act = append(act, token)
			}
			if !reflect.DeepEqual(act, test.out) {
				t.Errorf("Incorrent list of tokens read:\nexpected %q,\nactual   %q", test.out, act)
			}
		})
	}

}
