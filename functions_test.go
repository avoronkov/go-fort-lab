package main

import "testing"

func TestSwap(t *testing.T) {
	s := NewStack()
	s.Push(1)
	s.Push(2)
	Swap(s)
	if act, exp := s.String(), "2 1 "; act != exp {
		t.Errorf("Swap incorrect: expected %q, actual %q", exp, act)
	}
}

func BenchmarkSwap(b *testing.B) {
	s := NewStack()
	s.Push(1)
	s.Push(2)
	for i := 0; i < b.N; i++ {
		Swap(s)
	}
}

func TestRot(t *testing.T) {
	s := NewStack()
	s.Push(1)
	s.Push(2)
	s.Push(3)
	Rot(s)
	if act, exp := s.String(), "2 3 1 "; act != exp {
		t.Errorf("Swap incorrect: expected %q, actual %q", exp, act)
	}
}

func BenchmarkRot(b *testing.B) {
	s := NewStack()
	s.Push(1)
	s.Push(2)
	s.Push(3)
	for i := 0; i < b.N; i++ {
		Rot(s)
	}
}

func BenchmarkPlus(b *testing.B) {
	s := NewStack()
	s.Push(2)
	s.Push(1)
	for i := 0; i < b.N; i++ {
		Plus(s)
		s.Push(0)
	}
}

func BenchmarkMinus(b *testing.B) {
	s := NewStack()
	s.Push(2)
	s.Push(1)
	for i := 0; i < b.N; i++ {
		Minus(s)
		s.Push(0)
	}
}

func BenchmarkMultiply(b *testing.B) {
	s := NewStack()
	s.Push(2)
	s.Push(1)
	for i := 0; i < b.N; i++ {
		Multiply(s)
		s.Push(1)
	}
}

func BenchmarkLess(b *testing.B) {
	s := NewStack()
	s.Push(1)
	s.Push(2)
	for i := 0; i < b.N; i++ {
		Less(s)
		s.Push(2)
	}
}

func TestFunctions(t *testing.T) {
	testdata := []struct {
		name  string
		op    func(*Stack) error
		input []int
		exp   string
	}{
		{"Plus", Plus, []int{8, 5}, "13 "},
		{"Minus", Minus, []int{8, 5}, "3 "},
		{"Multiply", Multiply, []int{8, 5}, "40 "},
		{"Less(8, 5)", Less, []int{8, 5}, "0 "},
		{"Less(5, 5)", Less, []int{5, 5}, "0 "},
		{"Less(5, 8)", Less, []int{5, 8}, "1 "},
	}

	for _, test := range testdata {
		t.Run(test.name, func(t *testing.T) {
			s := &Stack{
				stack: test.input,
			}
			if err := test.op(s); err != nil {
				t.Fatalf("Operation %v failed: %v", test.name, err)
			}
			if act, exp := s.String(), test.exp; act != exp {
				t.Errorf("Incorrect operation %v: expected %q, actual %q", test.name, exp, act)
			}
		})
	}
}
