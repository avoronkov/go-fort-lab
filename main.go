package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

var (
	dumpStack bool
)

func init() {
	flag.BoolVar(&dumpStack, "dump-stack", false, "dump stack content after each operation")
	flag.BoolVar(&dumpStack, "d", false, "dump stack content after each operation (shorthand)")

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "StackPL - stack programming language.\n\n")
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()

		help := `
Supported operations:
- swap : a b -> b a
- dup  : a -> a a
- drop : a -> ()
- rot  : a b c -> b c a
- +    : a b -> a+b
- -    : a b -> a-b
- *    : a b -> a*b
- <    : a b -> a<b (true = 1, false = 0)
- !    : a -> !a (true -> 0, false -> 1)
- print : prints top value of stack
- dump  : prints whole stack
`
		fmt.Fprintf(flag.CommandLine.Output(), "%v", help)
	}
}

func main() {
	flag.Parse()

	var input io.Reader
	if len(flag.Args()) == 0 {
		input = os.Stdin
	} else {
		f, err := MultiFileReader(flag.Args()...)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		input = f
	}
	fort := NewInterpret(input, os.Stdout)
	if err := fort.Run(); err != nil {
		log.Fatal(err)
	}
}
