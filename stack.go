package main

import (
	"errors"
	"strconv"
)

const DefaultStackSize = 1024

type Stack struct {
	stack []int
}

func NewStack() *Stack {
	return &Stack{
		stack: make([]int, 0, DefaultStackSize),
	}
}

func (s Stack) String() (res string) {
	for _, x := range s.stack {
		res += strconv.Itoa(x) + " "
	}
	return
}

func (s Stack) Size() int {
	return len(s.stack)
}

// Push is always successful
func (s *Stack) Push(value ...int) error {
	s.stack = append(s.stack, value...)
	return nil
}

func (s *Stack) Pop() (int, error) {
	l := len(s.stack)
	if l == 0 {
		return 0, errors.New("Stack is empty")
	}
	value := s.stack[l-1]
	s.stack = s.stack[:l-1]
	return value, nil
}

func (s *Stack) Peek() (int, error) {
	l := len(s.stack)
	if l == 0 {
		return 0, errors.New("Stack is empty")
	}
	return s.stack[l-1], nil
}
