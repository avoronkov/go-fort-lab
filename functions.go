package main

import (
	"fmt"
	"io"
)

func Swap(s *Stack) error {
	b, _ := s.Pop()
	a, err := s.Pop()
	if err != nil {
		return err
	}
	return s.Push(b, a)
}

func Dup(s *Stack) error {
	a, err := s.Peek()
	if err != nil {
		return err
	}
	return s.Push(a)
}

func Drop(s *Stack) error {
	_, err := s.Pop()
	return err
}

// Rot  a b c -- b c a
func Rot(s *Stack) error {
	c, _ := s.Pop()
	b, _ := s.Pop()
	a, err := s.Pop()
	if err != nil {
		return err
	}
	return s.Push(b, c, a)
}

func Plus(s *Stack) error {
	b, _ := s.Pop()
	a, err := s.Pop()
	if err != nil {
		return err
	}
	return s.Push(a + b)
}

func Minus(s *Stack) error {
	b, _ := s.Pop()
	a, err := s.Pop()
	if err != nil {
		return err
	}
	return s.Push(a - b)
}

func Multiply(s *Stack) error {
	b, _ := s.Pop()
	a, err := s.Pop()
	if err != nil {
		return err
	}
	return s.Push(a * b)
}

func Less(s *Stack) error {
	b, _ := s.Pop()
	a, err := s.Pop()
	if err != nil {
		return err
	}
	res := 0
	if a < b {
		res = 1
	}
	return s.Push(res)
}

func Not(s *Stack) error {
	a, err := s.Pop()
	if err != nil {
		return err
	}
	if a > 0 {
		a = 0
	} else {
		a = 1
	}
	return s.Push(a)
}

func Print(s *Stack, w io.Writer) error {
	a, err := s.Peek()
	if err != nil {
		return err
	}
	fmt.Fprintln(w, a)
	return nil
}

func Dump(s *Stack, w io.Writer) error {
	fmt.Fprintln(w, s.String())
	return nil
}
